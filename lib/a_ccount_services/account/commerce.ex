defmodule ACcountServices.Account.Commerce do
  use Ecto.Schema
  import Ecto.Changeset

  alias ACcountServices.Account.User

  schema "commerces" do
    field :coordinates, {:array, :string}
    field :description, :string
    field :location, :string
    field :image, :string
    field :logo, :string
    field :name, :string
    field :phone, :string
    field :schedule, :map
    field :membership, :map
    field :type, {:array, :string}
    field :production, :boolean, default: false
    field :delivery_service, :boolean, default: false
    
    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(commerce, attrs) do
    commerce
    |> cast(attrs, [:production, :name, :phone, :delivery_service, :image, :logo, :membership, :schedule, :description, :type, :location, :coordinates, :user_id])
    |> validate_required([:production, :name, :phone, :delivery_service, :membership, :description, :type, :location, :coordinates, :user_id])
    |> unique_constraint(:name)
  end
end
