defmodule ACcountServices.Account.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias ACcountServices.Account.{ Credential, Commerce }
  
  schema "users" do
    field :a_bot, :boolean, default: false
    field :a_commerce, :boolean, default: true
    field :a_commerce_membership, :map
    field :a_lytics, :boolean, default: false
    field :lastname, :string
    field :username, :string
    field :name, :string
    
    has_one :credential, Credential
    has_one :commerce, Commerce

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :name, :lastname, :a_lytics, :a_commerce, :a_commerce_membership, :a_bot])
    |> validate_required([:username, :name, :lastname, :a_lytics, :a_commerce, :a_commerce_membership, :a_bot])
    |> unique_constraint(:username)
  end
end
