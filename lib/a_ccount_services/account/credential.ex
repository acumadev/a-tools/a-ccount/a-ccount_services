defmodule ACcountServices.Account.Credential do
  use Ecto.Schema
  import Ecto.Changeset

  alias ACcountServices.Account.User

  schema "credentials" do
    field :email, :string
    field :password, :string
    field :type, :string
    
    belongs_to :user, User
    
    timestamps()
  end

  @doc false
  def changeset(credential, attrs) do
    credential
    |> cast(attrs, [:email, :password, :type])
    |> validate_required([:email, :password, :type])
    |> unique_constraint(:email)
    |> encrypt_pass()
  end

  defp encrypt_pass(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password, Bcrypt.hash_pwd_salt(pass))
      _ -> changeset
    end
  end
end
