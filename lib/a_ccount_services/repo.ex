defmodule ACcountServices.Repo do
  use Ecto.Repo,
    otp_app: :a_ccount_services,
    adapter: Ecto.Adapters.Postgres
end
