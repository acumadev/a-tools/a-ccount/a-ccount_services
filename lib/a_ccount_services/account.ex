defmodule ACcountServices.Account do
  @moduledoc """
  The Account context.
  """

  import Ecto.Query, warn: false
  alias ACcountServices.Repo

  alias ACcountServices.Account.User
  alias ACcountServices.Account.Credential

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    User
    |> Repo.all()
    |> Repo.preload(:credential)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id) do 
    User
    |> Repo.get!(id)
    |> Repo.preload(:credential)
  end

  def get_login(username) do
    User
    |> Repo.get_by(username: username)
    |> Repo.preload(:credential)
  end

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Ecto.Changeset.cast_assoc(:credential, with: &Credential.changeset/2)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Ecto.Changeset.cast_assoc(:credential, with: &Credential.changeset/2)
    |> Repo.update()
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  @doc """
  Returns the list of credentials.

  ## Examples

      iex> list_credentials()
      [%Credential{}, ...]

  """
  def list_credentials do
    Repo.all(Credential)
  end

  @doc """
  Gets a single credential.

  Raises `Ecto.NoResultsError` if the Credential does not exist.

  ## Examples

      iex> get_credential!(123)
      %Credential{}

      iex> get_credential!(456)
      ** (Ecto.NoResultsError)

  """
  def get_credential!(id), do: Repo.get!(Credential, id)

  @doc """
  Creates a credential.

  ## Examples

      iex> create_credential(%{field: value})
      {:ok, %Credential{}}

      iex> create_credential(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_credential(attrs \\ %{}) do
    %Credential{}
    |> Credential.changeset(attrs)
    |> Ecto.Changeset.cast_assoc(:credential, with: &Credential.changeset/2)
    |> Repo.insert()
  end

  @doc """
  Updates a credential.

  ## Examples

      iex> update_credential(credential, %{field: new_value})
      {:ok, %Credential{}}

      iex> update_credential(credential, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_credential(%Credential{} = credential, attrs) do
    credential
    |> Credential.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a credential.

  ## Examples

      iex> delete_credential(credential)
      {:ok, %Credential{}}

      iex> delete_credential(credential)
      {:error, %Ecto.Changeset{}}

  """
  def delete_credential(%Credential{} = credential) do
    Repo.delete(credential)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking credential changes.

  ## Examples

      iex> change_credential(credential)
      %Ecto.Changeset{source: %Credential{}}

  """
  def change_credential(%Credential{} = credential) do
    Credential.changeset(credential, %{})
  end

  alias ACcountServices.Account.Commerce

  @doc """
  Returns the list of commerces.

  ## Examples

      iex> list_commerces()
      [%Commerce{}, ...]

  """
  def list_commerces(user_id) do
    Commerce
    |> where([commerce], commerce.user_id == ^user_id)
    |> Repo.all()
  end

  def list_all_commerces() do
    Commerce
    |> Repo.all()
  end

  def list_all_production_commerces() do
    Commerce
    |> where([commerce], commerce.production == true)
    |> Repo.all()
  end


  @doc """
  Gets a single commerce.

  Raises `Ecto.NoResultsError` if the Commerce does not exist.

  ## Examples

      iex> get_commerce!(123)
      %Commerce{}

      iex> get_commerce!(456)
      ** (Ecto.NoResultsError)

  """
  def get_commerce!(id,user_id) do
    Commerce
    |> where([commerce], commerce.user_id == ^user_id)
    |> Repo.get!(id)
  end

  def get_commerce_by(commerce_name) do
    Commerce
    |> Repo.get_by(name: commerce_name)
  end
  @doc """
  Creates a commerce.

  ## Examples

      iex> create_commerce(%{field: value})
      {:ok, %Commerce{}}

      iex> create_commerce(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_commerce(attrs \\ %{}) do
    %Commerce{}
    |> Commerce.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a commerce.

  ## Examples

      iex> update_commerce(commerce, %{field: new_value})
      {:ok, %Commerce{}}

      iex> update_commerce(commerce, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_commerce(%Commerce{} = commerce, attrs) do
    commerce
    |> Commerce.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a commerce.

  ## Examples

      iex> delete_commerce(commerce)
      {:ok, %Commerce{}}

      iex> delete_commerce(commerce)
      {:error, %Ecto.Changeset{}}

  """
  def delete_commerce(%Commerce{} = commerce) do
    Repo.delete(commerce)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking commerce changes.

  ## Examples

      iex> change_commerce(commerce)
      %Ecto.Changeset{source: %Commerce{}}

  """
  def change_commerce(%Commerce{} = commerce) do
    Commerce.changeset(commerce, %{})
  end
end
