defmodule ACcountServicesWeb.Router do
  use ACcountServicesWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug CORSPlug, origin: ["http://localhost:4200","http://54.200.130.180:4000","https://a-commerce.acuma.dev","https://dashboard.a-commerce.acuma.dev"]
    plug :accepts, ["json"]
  end

  scope "/", ACcountServicesWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", ACcountServicesWeb do
    pipe_through :api

    post    "/login", UserController, :login
    options "/login", UserController, :options

    get     "/commerces", CommerceController, :commerces
    options "/commerces", CommerceController, :options

    get     "/commerce/:commerce_name", CommerceController, :get_id
    options "/commerce/:commerce_name", CommerceController, :options

    options   "/users", UserController, :options
    resources "/users", UserController, except: [:new, :edit] do
      resources "/commerces", CommerceController, except: [:new, :edit]
      options   "/commerces", CommerceController, :options
      options   "/commerces/:id", CommerceController, :options
    end

    options   "/credentials", CredentialController, :options
    resources "/credentials", CredentialController, except: [:new, :edit]
    
  end
end
