defmodule ACcountServicesWeb.CommerceController do
  use ACcountServicesWeb, :controller

  alias ACcountServices.Account
  alias ACcountServices.Account.Commerce

  action_fallback ACcountServicesWeb.FallbackController

  def commerces(conn, _params) do
    commerces = Account.list_all_production_commerces()
    render(conn, "index.json", commerces: commerces)
  end

  def get_id(conn, %{"commerce_name" => commerce_name})do
    commerce = Account.get_commerce_by(commerce_name)
    render(conn, "id.json", commerce: commerce)
  end

  def index(conn, %{"user_id" => user_id}) do
    commerces = Account.list_commerces(user_id)
    render(conn, "index.json", commerces: commerces)
  end

  def create(conn, %{"commerce" => commerce_params, "user_id" => user_id}) do
    commerce_params = commerce_params
    |> Map.put("user_id", user_id)
    with {:ok, %Commerce{} = commerce} <- Account.create_commerce(commerce_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.user_commerce_path(conn, :show, user_id, commerce))
      |> render("show.json", commerce: commerce)
    end
  end

  def show(conn, %{"id" => id, "user_id" => user_id}) do
    commerce = Account.get_commerce!(id, user_id)
    render(conn, "show.json", commerce: commerce)
  end

  def update(conn, %{"id" => id, "commerce" => commerce_params, "user_id" => user_id}) do
    commerce = Account.get_commerce!(id, user_id)

    with {:ok, %Commerce{} = commerce} <- Account.update_commerce(commerce, commerce_params) do
      render(conn, "show.json", commerce: commerce)
    end
  end

  def delete(conn, %{"id" => id, "user_id" => user_id}) do
    commerce = Account.get_commerce!(id, user_id)

    with {:ok, %Commerce{}} <- Account.delete_commerce(commerce) do
      send_resp(conn, :no_content, "")
    end
  end
end
