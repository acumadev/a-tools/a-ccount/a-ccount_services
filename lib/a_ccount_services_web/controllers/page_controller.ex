defmodule ACcountServicesWeb.PageController do
  use ACcountServicesWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
