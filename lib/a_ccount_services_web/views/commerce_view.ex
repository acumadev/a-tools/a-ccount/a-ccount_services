defmodule ACcountServicesWeb.CommerceView do
  use ACcountServicesWeb, :view
  alias ACcountServicesWeb.CommerceView

  def render("index.json", %{commerces: commerces}) do
    %{data: render_many(commerces, CommerceView, "commerce.json")}
  end

  def render("show.json", %{commerce: commerce}) do
    %{data: render_one(commerce, CommerceView, "commerce.json")}
  end

  def render("id.json", %{commerce: commerce}) do
    %{id: commerce.id,
      logo: commerce.logo,
      delivery_service: commerce.delivery_service,
      phone: commerce.phone,
      location: commerce.location,
      coordinates: commerce.coordinates,
      schedule: commerce.schedule,
      }  
  end

  def render("commerce.json", %{commerce: commerce}) do
    %{id: commerce.id,
      name: commerce.name,
      logo: commerce.logo,
      type: commerce.type,
      image: commerce.image,
      phone: commerce.phone,
      location: commerce.location,
      schedule: commerce.schedule,
      production: commerce.production,
      membership: commerce.membership,
      description: commerce.description,
      coordinates: commerce.coordinates,
      delivery_service: commerce.delivery_service,
      }
  end
end
