defmodule ACcountServicesWeb.UserView do
  use ACcountServicesWeb, :view
  alias ACcountServicesWeb.{UserView, CredentialView}

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    credential = if Ecto.assoc_loaded?(user.credential) do
      render_one(user.credential, CredentialView, "email.json")
    end
    %{
      id: user.id,
      username: user.username,
      name: user.name,
      credential: credential,
      lastname: user.lastname,
      a_bot: user.a_bot,     
      a_lytics: user.a_lytics,
      a_commerce: user.a_commerce,
      a_commerce_membership: user.a_commerce_membership
    }
  end
end
