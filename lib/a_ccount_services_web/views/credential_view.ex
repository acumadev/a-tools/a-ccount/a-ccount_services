defmodule ACcountServicesWeb.CredentialView do
  use ACcountServicesWeb, :view
  alias ACcountServicesWeb.CredentialView

  def render("index.json", %{credentials: credentials}) do
    %{data: render_many(credentials, CredentialView, "credential.json")}
  end

  def render("show.json", %{credential: credential}) do
    %{data: render_one(credential, CredentialView, "credential.json")}
  end

  def render("credential.json", %{credential: credential}) do
    %{id: credential.id,
      email: credential.email,
      password: credential.password,
      type: credential.type}
  end

  def render("email.json", %{credential: credential}) do
    %{
      id: credential.id,
      email: credential.email,
      type: credential.type
    }
  end
end
