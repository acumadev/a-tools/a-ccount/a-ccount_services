defmodule ACcountServices.Repo.Migrations.CreateCredentials do
  use Ecto.Migration

  def change do
    create table(:credentials) do
      add :email, :string
      add :password, :text
      add :type, :string
      add :user_id, references(:users, on_delete: :delete_all), null: false
      
      timestamps()
    end
    create unique_index(:credentials, :email)
  end
end
