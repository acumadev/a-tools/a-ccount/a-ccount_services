defmodule ACcountServices.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :username, :string
      add :name, :string
      add :lastname, :string
      add :a_lytics, :boolean, default: false, null: false
      add :a_commerce, :boolean, default: false, null: true
      add :a_commerce_membership, :map
      add :a_bot, :boolean, default: false, null: false

      timestamps()
    end
    create unique_index(:users, :username)
  end
end
