defmodule ACcountServices.Repo.Migrations.CreateCommerces do
  use Ecto.Migration

  def change do
    create table(:commerces) do
      add :logo, :text
      add :image, :text
      add :name, :string
      add :phone, :string
      add :schedule, :map
      add :membership, :map
      add :location, :string
      add :description, :string
      add :type, {:array, :string}
      add :coordinates, {:array, :string}
      add :production, :boolean, default: false, null: false
      add :delivery_service, :boolean, default: false, null: false
      
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end

    create unique_index(:commerces, [:name])
    create index(:commerces, [:user_id])
  end
end
