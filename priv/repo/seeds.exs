# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     ACcountServices.Repo.insert!(%ACcountServices.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
# alias  ACcountServices.Account.User
# alias  ACcountServices.Repo

# Repo.insert! %User{
#   name: "Azul",
#   lastname: "Perez",
#   username: "Azulito",
#   a_commerce: false,
#   a_lytics: false,
#   a_bot: false
# }