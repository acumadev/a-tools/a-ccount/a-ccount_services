defmodule ACcountServices.AccountTest do
  use ACcountServices.DataCase

  alias ACcountServices.Account

  describe "users" do
    alias ACcountServices.Account.User

    @valid_attrs %{a_bot: true, a_commerce: true, a_lytics: true, lastname: "some lastname", name: "some name", username: "some username"}
    @update_attrs %{a_bot: false, a_commerce: false, a_lytics: false, lastname: "some updated lastname", name: "some updated name", username: "some updated username"}
    @invalid_attrs %{a_bot: nil, a_commerce: nil, a_lytics: nil, lastname: nil, name: nil, username: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Account.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Account.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Account.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Account.create_user(@valid_attrs)
      assert user.a_bot == true
      assert user.a_commerce == true
      assert user.a_lytics == true
      assert user.lastname == "some lastname"
      assert user.name == "some name"
      assert user.username == "some username"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Account.update_user(user, @update_attrs)
      assert user.a_bot == false
      assert user.a_commerce == false
      assert user.a_lytics == false
      assert user.lastname == "some updated lastname"
      assert user.name == "some updated name"
      assert user.username == "some updated username"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Account.update_user(user, @invalid_attrs)
      assert user == Account.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Account.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Account.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Account.change_user(user)
    end
  end

  describe "credentials" do
    alias ACcountServices.Account.Credential

    @valid_attrs %{email: "some email", password: "some password", type: "some type"}
    @update_attrs %{email: "some updated email", password: "some updated password", type: "some updated type"}
    @invalid_attrs %{email: nil, password: nil, type: nil}

    def credential_fixture(attrs \\ %{}) do
      {:ok, credential} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Account.create_credential()

      credential
    end

    test "list_credentials/0 returns all credentials" do
      credential = credential_fixture()
      assert Account.list_credentials() == [credential]
    end

    test "get_credential!/1 returns the credential with given id" do
      credential = credential_fixture()
      assert Account.get_credential!(credential.id) == credential
    end

    test "create_credential/1 with valid data creates a credential" do
      assert {:ok, %Credential{} = credential} = Account.create_credential(@valid_attrs)
      assert credential.email == "some email"
      assert credential.password == "some password"
      assert credential.type == "some type"
    end

    test "create_credential/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.create_credential(@invalid_attrs)
    end

    test "update_credential/2 with valid data updates the credential" do
      credential = credential_fixture()
      assert {:ok, %Credential{} = credential} = Account.update_credential(credential, @update_attrs)
      assert credential.email == "some updated email"
      assert credential.password == "some updated password"
      assert credential.type == "some updated type"
    end

    test "update_credential/2 with invalid data returns error changeset" do
      credential = credential_fixture()
      assert {:error, %Ecto.Changeset{}} = Account.update_credential(credential, @invalid_attrs)
      assert credential == Account.get_credential!(credential.id)
    end

    test "delete_credential/1 deletes the credential" do
      credential = credential_fixture()
      assert {:ok, %Credential{}} = Account.delete_credential(credential)
      assert_raise Ecto.NoResultsError, fn -> Account.get_credential!(credential.id) end
    end

    test "change_credential/1 returns a credential changeset" do
      credential = credential_fixture()
      assert %Ecto.Changeset{} = Account.change_credential(credential)
    end
  end

  describe "commerces" do
    alias ACcountServices.Account.Commerce

    @valid_attrs %{description: "some description", image: "some image", logo: "some logo", name: "some name", type: []}
    @update_attrs %{description: "some updated description", image: "some updated image", logo: "some updated logo", name: "some updated name", type: []}
    @invalid_attrs %{description: nil, image: nil, logo: nil, name: nil, type: nil}

    def commerce_fixture(attrs \\ %{}) do
      {:ok, commerce} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Account.create_commerce()

      commerce
    end

    test "list_commerces/0 returns all commerces" do
      commerce = commerce_fixture()
      assert Account.list_commerces() == [commerce]
    end

    test "get_commerce!/1 returns the commerce with given id" do
      commerce = commerce_fixture()
      assert Account.get_commerce!(commerce.id) == commerce
    end

    test "create_commerce/1 with valid data creates a commerce" do
      assert {:ok, %Commerce{} = commerce} = Account.create_commerce(@valid_attrs)
      assert commerce.description == "some description"
      assert commerce.image == "some image"
      assert commerce.logo == "some logo"
      assert commerce.name == "some name"
      assert commerce.type == []
    end

    test "create_commerce/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.create_commerce(@invalid_attrs)
    end

    test "update_commerce/2 with valid data updates the commerce" do
      commerce = commerce_fixture()
      assert {:ok, %Commerce{} = commerce} = Account.update_commerce(commerce, @update_attrs)
      assert commerce.description == "some updated description"
      assert commerce.image == "some updated image"
      assert commerce.logo == "some updated logo"
      assert commerce.name == "some updated name"
      assert commerce.type == []
    end

    test "update_commerce/2 with invalid data returns error changeset" do
      commerce = commerce_fixture()
      assert {:error, %Ecto.Changeset{}} = Account.update_commerce(commerce, @invalid_attrs)
      assert commerce == Account.get_commerce!(commerce.id)
    end

    test "delete_commerce/1 deletes the commerce" do
      commerce = commerce_fixture()
      assert {:ok, %Commerce{}} = Account.delete_commerce(commerce)
      assert_raise Ecto.NoResultsError, fn -> Account.get_commerce!(commerce.id) end
    end

    test "change_commerce/1 returns a commerce changeset" do
      commerce = commerce_fixture()
      assert %Ecto.Changeset{} = Account.change_commerce(commerce)
    end
  end
end
