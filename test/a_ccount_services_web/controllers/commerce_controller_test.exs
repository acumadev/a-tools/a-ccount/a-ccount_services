defmodule ACcountServicesWeb.CommerceControllerTest do
  use ACcountServicesWeb.ConnCase

  alias ACcountServices.Account
  alias ACcountServices.Account.Commerce

  @create_attrs %{
    description: "some description",
    image: "some image",
    logo: "some logo",
    name: "some name",
    type: []
  }
  @update_attrs %{
    description: "some updated description",
    image: "some updated image",
    logo: "some updated logo",
    name: "some updated name",
    type: []
  }
  @invalid_attrs %{description: nil, image: nil, logo: nil, name: nil, type: nil}

  def fixture(:commerce) do
    {:ok, commerce} = Account.create_commerce(@create_attrs)
    commerce
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all commerces", %{conn: conn} do
      conn = get(conn, Routes.commerce_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create commerce" do
    test "renders commerce when data is valid", %{conn: conn} do
      conn = post(conn, Routes.commerce_path(conn, :create), commerce: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.commerce_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some description",
               "image" => "some image",
               "logo" => "some logo",
               "name" => "some name",
               "type" => []
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.commerce_path(conn, :create), commerce: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update commerce" do
    setup [:create_commerce]

    test "renders commerce when data is valid", %{conn: conn, commerce: %Commerce{id: id} = commerce} do
      conn = put(conn, Routes.commerce_path(conn, :update, commerce), commerce: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.commerce_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some updated description",
               "image" => "some updated image",
               "logo" => "some updated logo",
               "name" => "some updated name",
               "type" => []
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, commerce: commerce} do
      conn = put(conn, Routes.commerce_path(conn, :update, commerce), commerce: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete commerce" do
    setup [:create_commerce]

    test "deletes chosen commerce", %{conn: conn, commerce: commerce} do
      conn = delete(conn, Routes.commerce_path(conn, :delete, commerce))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.commerce_path(conn, :show, commerce))
      end
    end
  end

  defp create_commerce(_) do
    commerce = fixture(:commerce)
    {:ok, commerce: commerce}
  end
end
